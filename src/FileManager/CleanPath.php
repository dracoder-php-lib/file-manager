<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 18:20
 */

namespace App\Util\FileManagement;

/**
 * Class CleanPath
 *
 * Get a clean path
 * e.g.: '\\C/Documents///uploads' -> '/C/Documents/uploads'
 */
class CleanPath
{
    /** @var string $path */
    private $path;

    /**
     * CleanPath constructor.
     *
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = static::cleanPath($path);
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Remove repeated slashes
     * e.g.: /my//path///file.png -> my/path/file.png
     *
     * @param string $needle
     *
     * @return string
     */
    private static function removeRepeatedSlash(string $needle): string
    {
        return preg_replace('#/+#', '/', $needle);
    }

    /**
     * @param $needle
     *
     * @return string
     */
    private static function replaceBackslashWithSlash(string $needle): string
    {
        return preg_replace('/\\\\/', '/', $needle);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private static function cleanPath(string $path): string
    {
        $path = static::replaceBackslashWithSlash($path);
        return static::removeRepeatedSlash($path);
    }
}