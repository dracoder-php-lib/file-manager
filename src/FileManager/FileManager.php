<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 19:24
 */

namespace App\Util\FileManagement;


class FileManager
{
    /** @var string $projectDir */
    protected $projectDir;

    /**
     * Upload constructor.
     *
     * @param string $projectDir
     */
    public function __construct($projectDir)
    {
        $this->projectDir = $projectDir;
    }
}