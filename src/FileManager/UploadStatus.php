<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 17:00
 */

namespace App\Util\FileManagement;


class UploadStatus
{
    public const OK = 1;
    public const NOT_UPLOADED = 101;
    public const UPLOADING_ERROR = 102;
    public const INVALID_DIMENSIONS = 103;
    public const INVALID_FORMAT = 104;
    public const UNKNOWN = 105;

    /** @var int $value */
    private $value;

    /**
     * UploadStatus constructor.
     *
     * @param int $status
     */
    public function __construct($status)
    {
        $this->value = $status;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return ($this->value > 100);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->value === UploadStatus::OK) {
            return 'upload_correct';
        } elseif ($this->value === UploadStatus::NOT_UPLOADED) {
            return 'the_file_has_not_been_uploaded';
        } elseif ($this->value === UploadStatus::UPLOADING_ERROR) {
            return 'there_was_an_error_uploading_the_file';
        } elseif ($this->value === UploadStatus::INVALID_DIMENSIONS) {
            return 'the_image_exceeds_the_allowed_dimensions';
        } elseif ($this->value === UploadStatus::INVALID_FORMAT) {
            return 'invalid_format';
        } elseif ($this->value === UploadStatus::UNKNOWN) {
            return 'unknown_error';
        } else {
            return '';
        }
    }
}