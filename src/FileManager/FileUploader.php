<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 15:56
 */

namespace App\Util\FileManagement;


use App\Util\DateTimeExtension;
use App\Util\ExceptionHandler;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileUploader
 */
class FileUploader extends FileManager
{
    /** @var UploadStatus $status */
    protected $status;

    /** @var string $prefix */
    protected $prefix = '';

    /** @var string $postfix */
    protected $postfix = '';

    /** @var string $folder */
    protected $folder = '';

    /** @var string $filename */
    protected $wantedFilename;

    /** @var string $uploadedFilename */
    protected $uploadedFilename;

    /** @var array $allowedExtensions */
    protected $allowedExtensions;

    /**
     * Upload constructor.
     *
     * @param string $projectDir
     * @param array $allowedExtensions
     */
    public function __construct($projectDir, $allowedExtensions)
    {
        $this->status = new UploadStatus(UploadStatus::NOT_UPLOADED);
        $this->allowedExtensions = $allowedExtensions;

        parent::__construct($projectDir);
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    /**
     * @return string
     */
    public function getPostfix()
    {
        return $this->postfix;
    }

    /**
     * @param $postfix
     */
    public function setPostfix($postfix)
    {
        $this->postfix = $postfix;
    }

    /**
     * @param string $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->wantedFilename = $filename;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return (string)$this->status;
    }

    /**
     * @return string
     */
    public function getUploadedAbsolutePath()
    {
        if (!$this->haveErrors()) {
            $path = $this->projectDir . '/' . $this->folder . '/' . $this->uploadedFilename;

            return (new CleanPath($path))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @return bool
     */
    public function haveErrors()
    {
        return $this->status->isError();
    }

    /**
     * @return string
     */
    public function getUploadedFilename()
    {
        if (!$this->haveErrors()) {
            $path = $this->uploadedFilename;

            return (new CleanPath($path))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @param UploadedFile $file
     *
     * @return string|false
     */
    public function upload(UploadedFile $file)
    {
        $this->status = new UploadStatus(UploadStatus::NOT_UPLOADED);

        $extension = $file->guessExtension();
        if (!in_array($file->guessExtension(), $this->allowedExtensions)) {
            if (in_array($file->getClientOriginalExtension(), $this->allowedExtensions)) {
                $extension = $file->getClientOriginalExtension();
            } else {
                $this->status = new UploadStatus(UploadStatus::INVALID_FORMAT);

                return false;
            }
        }

        $absoluteDir = sprintf('%s/%s', $this->projectDir, $this->folder);
        $absoluteDir = (new CleanPath($absoluteDir))->getPath();

        if ($this->wantedFilename) {
            $filename = sprintf('%s.%s', $this->wantedFilename, $extension);
        } else {
            $now = new DateTimeExtension();
            if (!$now) {
                $this->status = new UploadStatus(UploadStatus::UPLOADING_ERROR);
                return false;
            }
            $filename = sprintf('%s%s%s.%s', $this->prefix, $now->format('Y-m-d_H.i.s.u'), $this->postfix, $extension);
        }

        try {
            $file->move($absoluteDir, $filename);
            $this->uploadedFilename = $filename;
            $this->status = new UploadStatus(UploadStatus::OK);

            return $this->getUploadedRelativePath();
        } catch (FileException $e) {
            (new ExceptionHandler($e))->handle(false);
            $this->status = new UploadStatus(UploadStatus::UPLOADING_ERROR);

            return false;
        }
    }

    /**
     * @return string
     */
    public function getUploadedRelativePath()
    {
        if (!$this->haveErrors()) {
            $path = $this->folder . '/' . $this->uploadedFilename;

            return (new CleanPath($path))->getPath();
        } else {
            return '';
        }
    }
}