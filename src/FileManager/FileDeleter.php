<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 18:18
 */

namespace App\Util\FileManagement;


class FileDeleter extends FileManager
{
    /**
     * @param string $relativePath
     */
    public function delete($relativePath)
    {
        $absolutePath = sprintf('%s/%s', $this->projectDir, $relativePath);
        $absolutePath = (new CleanPath($absolutePath))->getPath();

        @unlink($absolutePath);
    }
}