<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 19:22
 */

namespace App\Util\FileManagement;

class Thumbnail extends FileManager
{
    /** @var string $relativePath */
    private $relativePath;

    /**
     * Thumbnail constructor.
     *
     * @param string $projectDir
     * @param string $relativePath
     */
    public function __construct(string $projectDir, $relativePath)
    {
        $this->relativePath = $relativePath;

        parent::__construct($projectDir);
    }

    /**
     * @param int $size
     * @param string $measurement
     *
     * @return string route of the thumbnail
     */
    public function getThumbnail($size, $measurement = ImageDimension::HEIGHT)
    {
        if ($measurement !== ImageDimension::WIDTH) {
            $measurement = ImageDimension::HEIGHT;
        }

        $this->createFolder($this->getAbsoluteThumbnailsFolder());

        $this->createThumbnail($size, $measurement);

        return $this->getRelativeThumbnailPath($size, $measurement);
    }

    /**
     * @return string
     */
    private function getAbsoluteOriginalPath()
    {
        if ($this->projectDir && $this->relativePath) {
            $absolutePath = sprintf('%s/%s', $this->projectDir, $this->relativePath);

            return (new CleanPath($absolutePath))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @param string $measurement ('h'|'w')
     * @param int $size
     *
     * @return string
     */
    private function getRelativeThumbnailPath($size, $measurement = ImageDimension::HEIGHT)
    {
        if ($measurement !== ImageDimension::WIDTH) {
            $measurement = ImageDimension::HEIGHT;
        }

        if ($this->getRelativeThumbnailsFolder()) {
            $pathInfo = pathinfo($this->relativePath);
            $filename = $pathInfo['basename'];
            $newName = sprintf('%s%d_%s', $measurement, $size, $filename);
            $relativePath = sprintf('%s/%s', $this->getRelativeThumbnailsFolder(), $newName);

            return (new CleanPath($relativePath))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    private function getRelativeThumbnailsFolder()
    {
        if ($this->projectDir && $this->relativePath) {
            $pathInfo = pathinfo($this->relativePath);
            $thumbnailsFolder = $pathInfo['filename'];
            $dirName = $pathInfo['dirname'];
            $relativeThumbnailFolder = sprintf('%s/%s', $dirName, $thumbnailsFolder);
            return (new CleanPath($relativeThumbnailFolder))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @param string $measurement ('h'|'w')
     * @param int $size
     *
     * @return string
     */
    private function getAbsoluteThumbnailPath($size, $measurement = ImageDimension::HEIGHT)
    {
        if ($measurement !== ImageDimension::WIDTH) {
            $measurement = ImageDimension::HEIGHT;
        }

        if ($this->projectDir && $this->relativePath) {
            $absolutePath = sprintf('%s/%s', $this->projectDir, $this->getRelativeThumbnailPath($size, $measurement));

            return (new CleanPath($absolutePath))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @return string
     */
    private function getAbsoluteThumbnailsFolder()
    {
        if ($this->projectDir && $this->relativePath) {
            $thumbnailsAbsoluteFolderPath = sprintf('%s/%s', $this->projectDir, $this->getRelativeThumbnailsFolder());

            return (new CleanPath($thumbnailsAbsoluteFolderPath))->getPath();
        } else {
            return '';
        }
    }

    /**
     * @param int $size
     * @param string $measurement
     */
    private function createThumbnail($size, $measurement = ImageDimension::HEIGHT)
    {
        if ($measurement !== ImageDimension::WIDTH) {
            $measurement = ImageDimension::HEIGHT;
        }

        $thumbnailsAbsolutePath = $this->getAbsoluteThumbnailPath($size, $measurement);

        if (!file_exists($thumbnailsAbsolutePath)) {
            $mi = new ModifiedImage($this->getAbsoluteOriginalPath(), true);
            if ($measurement === ImageDimension::WIDTH) {
                $mi->resizeToWidth($size);
            } else {
                $mi->resizeToHeight($size);
            }
            $mi->save($thumbnailsAbsolutePath);
        }
    }

    /**
     * @param string $thumbnailsAbsoluteFolderPath
     */
    private function createFolder($thumbnailsAbsoluteFolderPath)
    {
        $thumbnailsAbsoluteFolderPath = (new CleanPath($thumbnailsAbsoluteFolderPath))->getPath();

        if (!is_dir($thumbnailsAbsoluteFolderPath)) {
            mkdir($thumbnailsAbsoluteFolderPath);
        }
    }
}